﻿using Telegram.Bot.Types;

namespace AdCleanerBot.Models
{
    public class Users
    {
        public int Id { get; set; }
        public Type Type { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string LanguageCode { get; set; }
        public Users() { }
        public Users(User user, Type type)
        {
            this.Id = user.Id;
            this.Type = type;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Username = user.Username;
            this.LanguageCode = user.LanguageCode;
        }
    }
}
