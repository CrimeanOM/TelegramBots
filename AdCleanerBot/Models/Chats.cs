﻿using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace AdCleanerBot.Models
{
    public class Chats
    {
        public long Id { get; set; }
        public Type myType { get; set; }
        public ChatType Type { get; set; }
        public string Title { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Chats() { }
        public Chats(Chat chat, Type type)
        {
            this.Id = chat.Id;
            this.myType = type;
            this.Type = chat.Type;
            this.Title = chat.Title;
            this.Username = chat.Username;
            this.FirstName = chat.FirstName;
            this.LastName = chat.LastName;
        }
    }
}
