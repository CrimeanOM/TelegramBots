﻿
namespace AdCleanerBot.Models
{
    public class Keywords
    {
        public long Id { get; set; }
        public string Keyword { get; set; }
        public Type Type { get; set; }
        public Keywords() { }
        public Keywords(string keyword, Type type)
        {
            this.Keyword = keyword;
            this.Type = type;
        }
    }
}
