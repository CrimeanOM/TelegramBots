﻿

namespace AdCleanerBot.Models
{
    public enum Type
    {
        Group = 0,
        Forward = 1,
        Wanted = 2,
        SpamerChat = 3,
        NormalChat = 4,
        Admin = 5,
        Spamer = 6
    }
}

