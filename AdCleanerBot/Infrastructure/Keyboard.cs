﻿using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.ReplyMarkups;

namespace AdCleanerBot.Infrastructure
{
    class Keyboard
    {
        public InlineKeyboardMarkup key { get; set; }

        public void Add(string button1, string button2, string button3, string button4, string button5)
        {
            var keyboard = new InlineKeyboardMarkup(new[]
            {
                new []
                {
                    new InlineKeyboardCallbackButton(button1, button1),
                    new InlineKeyboardCallbackButton(button2, button2),
                },
                new []
                {
                    new InlineKeyboardCallbackButton(button3, button3),
                    new InlineKeyboardCallbackButton(button4, button4),
                },
                  new []
                {
                    new InlineKeyboardCallbackButton(button5, button5),
                }
            });
            key = keyboard;
        }
        public void Add(string button1, string button2, string button3, string button4)
        {
            var keyboard = new InlineKeyboardMarkup(new[]
            {
                new []
                {
                    new InlineKeyboardCallbackButton(button1, button1),
                    new InlineKeyboardCallbackButton(button2, button2),
                },
                new []
                {
                    new InlineKeyboardCallbackButton(button3, button3),
                    new InlineKeyboardCallbackButton(button4, button4),
                }
            });
            key = keyboard;
        }
        public void Add(string button1, string button2, string button3)
        {
            var keyboard = new InlineKeyboardMarkup(new[]
            {
                new []
                {
                    new InlineKeyboardCallbackButton(button1, button1),
                    new InlineKeyboardCallbackButton(button2, button2),
                },
                new []
                {
                    new InlineKeyboardCallbackButton(button3, button3),
                }
            });
            key = keyboard;
        }
        public void Add(string button1, string button2)
        {
            var keyboard = new InlineKeyboardMarkup(new[]
          {
                new []
                {
                    new InlineKeyboardCallbackButton(button1, button1),
                    new InlineKeyboardCallbackButton(button2, button2),
                },
            });
            key = keyboard;
        }
        public void Add(string button1)
        {
            var keyboard = new InlineKeyboardMarkup(new[]
          {
                new []
                {
                    new InlineKeyboardCallbackButton(button1, button1),
                },
            });
            key = keyboard;
        }
    }
}
