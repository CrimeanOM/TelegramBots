﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Threading;

using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.InputMessageContents;
using Telegram.Bot.Types.ReplyMarkups;

using AdCleanerBot.Infrastructure;

namespace AdCleanerBot
{
    class Program
    {
        public static readonly TelegramBotClient Bot = new TelegramBotClient("telegramtoken");

        static void Main(string[] args)
        {
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnUpdate += BotOnUpdateRecieved;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            Bot.StartReceiving();

            while (true)
            {
                Thread.Sleep(500);
            }
        }

        static List<long> kwdAddMode = new List<long>();
        static List<long> kwdRemoveMode = new List<long>();
        static List<long> gKwdAddMode = new List<long>();
        static List<long> gKwdRemoveMode = new List<long>();
        static List<long> wantedAddMode = new List<long>();
        static List<long> wantedRemoveMode = new List<long>();
        static List<long> adminAddMode = new List<long>();
        static List<long> adminRemoveMode = new List<long>();

        static List<long> messages = new List<long>();
        static List<long> groups = new List<long>();
        static List<long> deck = new List<long>();
        static List<long> unban = new List<long>();

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var me = await Bot.GetMeAsync();
            var message = messageEventArgs.Message;
            var reply = message.ReplyToMessage;
            var context = new DBcontext();
            var admins = context.Users.Where(a => a.Type == Models.Type.Admin);
            var chats = context.Chats.Where(a => a.myType == Models.Type.NormalChat);
            var keywords = context.Keywords;
            var spamers = context.Users.Where(a => a.Type == Models.Type.Spamer);

            if (message == null) return;

            //Common commands
            if (message.Text == "/ping")
            {
                await Bot.SendTextMessageAsync(message.Chat.Id, "Pong");
            }

            //Private chats
            if (message.Chat.Type == ChatType.Private)
            {
                if (message.From.Username == "s0mename")
                {
                    if (message.Text == "/usage")
                    {
                        string helpmsg = $@"/newAdmin
/removeAdmin <i>@adminusername</i>
/admins
/editchatlist
/editchanlist";
                        await Bot.SendTextMessageAsync(message.Chat.Id, helpmsg, ParseMode.Html, true);
                    }
                    else if (message.Text.StartsWith("/newAdmin"))
                    {
                        string msg = "Ок, форвардни мне сообщение от пользователя которого хотите сделать администратором.";
                        await Bot.SendTextMessageAsync(message.Chat.Id, msg);
                    }
                    else if (message.Text.StartsWith("/removeAdmin"))
                    {
                        string msg = "Ок, форвардни мне сообщение от пользователя которого хотите удалить из списка администраторов.";
                        await Bot.SendTextMessageAsync(message.Chat.Id, msg);
                    }
                    else if (message.Text == "/admins")
                    {
                        string result = "";
                        foreach (var admin in admins)
                        {
                            var name = admin.Username == null ? $"{admin.FirstName} {admin.LastName}" : $"{admin.Username}";
                            result += $"{name}\n";
                        }
                        if (!string.IsNullOrWhiteSpace(result))
                            await Bot.SendTextMessageAsync(message.Chat.Id, result);
                    }
                }
                if (admins.Any(a => a.Id == message.From.Id))
                {
                    if (message.Text == "/help")
                    {
                        string usage = $@"reply + /kill - Ban user from all chats.
reply + /burn - Ban user from all chats.
reply + /clean - Remove message.
reply + /zaebal [minute_count] - Restrict user to send media and stickers (default: 30 minutes).
reply + /mute [minute_count] - Restrict user to readonly (default: 30 minutes).
reply + /unmute - Remove all restrictions from user.
/chatlist - Chat list.
/chanlist - Channel list.
/start - Menu.
/rules - Rules.
/chats - Top chats.
/frags - Frag count.";
                        await Bot.SendTextMessageAsync(message.Chat.Id, usage);
                    }
                    else if (message.Text == "/start")
                    {
                        var keyboard = new InlineKeyboardMarkup(new[]
                        {
                                new []
                                {
                                    new InlineKeyboardCallbackButton("Сообщения", "Сообщения"),
                                    new InlineKeyboardCallbackButton("Группы", "Группы"),
                                },
                                new []
                                {
                                    new InlineKeyboardCallbackButton("Доска почета", "Доска почета"),
                                    new InlineKeyboardCallbackButton("Освободить", "Освободить"),
                                }
                         });
                        await Bot.SendTextMessageAsync(message.Chat.Id, $"Настройки @{me.Username}", replyMarkup: keyboard);
                    }
                    else if (message.Text == "/chats")
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Just a sec...");
                        string report = "Chats, where members count > 300:\n";

                        int total = 0;

                        Dictionary<string, int> dict = new Dictionary<string, int>();

                        foreach (var chat in chats)
                        {
                            try
                            {
                                int members = await Bot.GetChatMembersCountAsync(chat.Id);
                                if (members > 300 && chat.Username != null)
                                    dict.Add(chat.Username, members);
                                total += members;
                            }
                            catch (Telegram.Bot.Exceptions.ApiRequestException)
                            {
                                Database.ChatsRemove(chat.Id);
                            }
                        }

                        foreach (var pair in dict.OrderByDescending(pair => pair.Value))
                        {
                            report += $"@{pair.Key} - {pair.Value}\n";
                        }

                        report += $"Total users: {total}";

                        await Bot.SendTextMessageAsync(message.Chat.Id, report); ;
                    }
                    else if (message.Text == "/frags")
                    {
                        int frags = spamers.Count();
                        string msg = $"Total frags: {frags}";
                        await Bot.SendTextMessageAsync(message.Chat.Id, msg);
                    }
                }

                foreach (var id in kwdAddMode)
                {
                    if (!message.Text.StartsWith("/") && id == message.From.Id)
                    {
                        var keyboard = new InlineKeyboardMarkup(new[]
                        {
                                new []
                                {
                                    new InlineKeyboardCallbackButton("Закончить", "Закончить"),
                                }
                        });
                        if (keywords.Any(a => a.Keyword == message.Text.ToLower()))
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Такая фраза уже есть!\nРежим ввода ключевых слов для сообщений.\nВведите слово или фразу:", replyMarkup: keyboard);
                        else
                        {
                            Database.KeywordsAdd(message.Text.ToLower(), Models.Type.Group);
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Добавлено.\nРежим ввода ключевых слов для сообщений.\nВведите слово или фразу:", replyMarkup: keyboard);
                        }
                    }
                }
                foreach (var id in kwdRemoveMode)
                {
                    if (!message.Text.StartsWith("/") && id == message.From.Id)
                    {
                        string usage = $"Режим удаления ключевых слов для сообщений\nВведите слово или фразу. Которое нужно удалить.";
                        var keyboard = new InlineKeyboardMarkup(new[]
                        {
                                new []
                                {
                                    new InlineKeyboardCallbackButton("Закончить", "Закончить"),
                                }
                        });
                        if (keywords.Any(a => a.Keyword == message.Text.ToLower()))
                        {
                            Database.KeywordsAdd(message.Text.ToLower(), Models.Type.Group);
                            
                            await Bot.SendTextMessageAsync(message.Chat.Id, $"Фраза удалена.\n{usage}", replyMarkup: keyboard);
                        }
                        else
                            await Bot.SendTextMessageAsync(message.Chat.Id, $"Нет такой фразы!\n{usage}", replyMarkup: keyboard);
                    }
                }

                foreach (var id in gKwdAddMode)
                {
                    if (!message.Text.StartsWith("/") && id == message.From.Id)
                    {
                        var keyboard = new InlineKeyboardMarkup(new[]
                        {
                                new []
                                {
                                    new InlineKeyboardCallbackButton("Закончить", "Закончить"),
                                }
                        });
                        if (keywords.Any(a => a.Keyword == message.Text.ToLower() && a.Type == Models.Type.Forward))
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Такая фраза уже есть!\nРежим ввода ключевых слов для групп\nВведите название чата или канала:", replyMarkup: keyboard);
                        else
                        {
                            Database.KeywordsAdd(message.Text.ToLower(), Models.Type.Forward);
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Добавлено.\nРежим ввода ключевых слов для групп\nВведите название чата или канала:", replyMarkup: keyboard);
                        }
                    }
                }
                foreach (var id in gKwdRemoveMode)
                {
                    if (!message.Text.StartsWith("/") && id == message.From.Id)
                    {
                        string usage = $"Режим удаления ключевых слов для групп\nВведите название группы. Которое нужно удалить.";
                        var keyboard = new InlineKeyboardMarkup(new[]
                        {
                                new []
                                {
                                    new InlineKeyboardCallbackButton("Закончить", "Закончить"),
                                }
                        });
                        if (keywords.Any(a => a.Keyword == message.Text.ToLower() && a.Type == Models.Type.Forward))
                        {
                            Database.KeywordsRemove(message.Text.ToLower(), Models.Type.Forward); 
                            await Bot.SendTextMessageAsync(message.Chat.Id, $"Фраза удалена.\n{usage}", replyMarkup: keyboard);
                        }
                        else
                            await Bot.SendTextMessageAsync(message.Chat.Id, $"Нет такой фразы!\n{usage}", replyMarkup: keyboard);
                    }
                }

                foreach (var id in wantedAddMode)
                {
                    if (!message.Text.StartsWith("/") && id == message.From.Id)
                    {
                        var keyboard = new InlineKeyboardMarkup(new[]
                        {
                                new []
                                {
                                    new InlineKeyboardCallbackButton("Закончить", "Закончить"),
                                }
                        });
                        string usage = $"Режим ввода ориентировок\nВведите username:";
                        var usr = message.Text.TrimStart('@').ToLower();
                        if (keywords.Any(a => a.Keyword == usr && a.Type == Models.Type.Wanted))
                            await Bot.SendTextMessageAsync(message.Chat.Id, $"Этот username уже есть на доске почета!\n{usage}", replyMarkup: keyboard);
                        else
                        {
                            Database.KeywordsAdd(usr, Models.Type.Wanted);
                            await Bot.SendTextMessageAsync(message.Chat.Id, $"Username добавлен.\n{usage}", replyMarkup: keyboard);
                        }
                    }
                }
                foreach (var id in wantedRemoveMode)
                {
                    if (!message.Text.StartsWith("/") && id == message.From.Id)
                    {
                        var keyboard = new InlineKeyboardMarkup(new[]
                        {
                                new []
                                {
                                    new InlineKeyboardCallbackButton("Закончить", "Закончить"),
                                }
                        });
                        string usage = "Введите username. Которое нужно удалить.";
                        var usr = message.Text.TrimStart('@').ToLower();
                        if (keywords.Any(a => a.Keyword == usr && a.Type == Models.Type.Wanted))
                        {
                            Database.KeywordsRemove(usr, Models.Type.Wanted);
                            await Bot.SendTextMessageAsync(message.Chat.Id, $"Ориентировка удалена.\n{usage}", replyMarkup: keyboard);
                        }
                        else
                            await Bot.SendTextMessageAsync(message.Chat.Id, $"Я такого не разыскиваю.\n{usage}", replyMarkup: keyboard);
                    }
                }

                foreach (var id in adminAddMode)
                {
                    if (message.ForwardFrom != null)
                    {
                        Database.UserAdd(message.ForwardFrom, Models.Type.Admin);
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Админ добавлен!");
                        adminAddMode.Remove(id);
                    }
                }
                foreach (var id in adminRemoveMode)
                {
                    if (message.ForwardFrom != null)
                    {
                        Database.UserRemove(message.ForwardFrom, Models.Type.Admin);
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Админ удален!");
                        adminAddMode.Remove(id);
                    }
                }
            }

            //Supergroups work
            else if (message.Chat.Type == ChatType.Supergroup)
            {
                //Adding new chats
                if (!chats.Any(a => a.Id == message.Chat.Id))
                    Database.ChatAdd(message.Chat, Models.Type.NormalChat);

                var adms = await Bot.GetChatAdministratorsAsync(message.Chat.Id);
                bool isMeAdmin = adms.Any(a => a.User.Id.ToString() == me.Id.ToString());

                //Commands for chat administrators
                if (reply != null && adms.Any(a => a.User.Id == message.From.Id) && isMeAdmin && !admins.Any(a => a.Id == message.From.Id))
                {
                    if (message.Text == "/kill")
                    {
                        try
                        {
                            await Bot.DeleteMessageAsync(message.Chat.Id, reply.MessageId);
                            await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                            await Bot.KickChatMemberAsync(message.Chat.Id, reply.From.Id);

                            string admin = message.From.Username != null ? $"@{message.From.Username}" : $"{message.From.FirstName} {message.From.LastName}";
                            string victim = reply.From.Username != null ? $"@{reply.From.Username}" : $"{reply.From.FirstName} {message.From.LastName}";
                            string msg = $"{admin} убил {victim} в @{message.Chat.Username}";
                            await Bot.SendTextMessageAsync(448868194, msg, ParseMode.Markdown);
                        }
                        catch (Telegram.Bot.Exceptions.ApiRequestException ex) { }
                    }
                    else if (message.Text.StartsWith("/mute"))
                    {
                        try
                        {
                            int time = 0;
                            var words = message.Text.Split();

                            if (words.Length >= 2)
                            {
                                try
                                {
                                    time = Int32.Parse(words[1]);
                                }
                                catch (FormatException) { }
                            }
                            else
                                time = 30;

                            var date = DateTime.Now.AddMinutes(time);
                            await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                            await Bot.RestrictChatMemberAsync(message.Chat.Id, reply.From.Id, date, false, false, false, false);
                            string name = reply.From.Username != null ? $"@{reply.From.Username}" : $"[{reply.From.FirstName}](tg://user?id={reply.From.Id})";
                            string msg = $"Уважаемый(ая) {name}!\nВам нужно посидеть в readonly {time} минут(ы).";
                            await Bot.SendTextMessageAsync(message.Chat.Id, msg, ParseMode.Markdown);
                        }
                        catch (Exception ex)
                        {
                            string result = $"{ex.Message}\nFROM:{message.Chat.FirstName}\nCOMMAND:/mute\nTO:{reply.From.FirstName} {reply.From.LastName}";
                            await Bot.SendTextMessageAsync(448868194, result);
                        }
                    }
                    else if (message.Text.StartsWith("/zaebal"))
                    {
                        try
                        {
                            int time = 0;
                            var words = message.Text.Split();
                            if (words.Length >= 2)
                            {
                                try
                                {
                                    time = Int32.Parse(words[1]);
                                }
                                catch (FormatException) { }
                            }
                            else
                                time = 30;

                            var date = DateTime.Now.AddMinutes(time);
                            await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                            await Bot.RestrictChatMemberAsync(message.Chat.Id, reply.From.Id, date, true, false, false, false);

                            string name = reply.From.Username != null ? $"@{reply.From.Username}" : $"[{reply.From.FirstName}](tg://user?id={reply.From.Id})";
                            string msg = $"Уважаемый(ая) {name}!\nВы заебали кидать стикеры/картинки не в тему, подумайте над своим поведением {time} минут(ы)!";
                            await Bot.SendTextMessageAsync(message.Chat.Id, msg, ParseMode.Markdown);
                        }
                        catch (Exception ex)
                        {
                            string result = $"{ex.Message}\nFROM:{message.Chat.FirstName}\nCOMMAND:/zaebal\nTO:{reply.From.FirstName} {reply.From.LastName}";
                            await Bot.SendTextMessageAsync(448868194, result);
                        }
                    }
                    else if (message.Text == "/unmute")
                    {
                        await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                        await Bot.RestrictChatMemberAsync(message.Chat.Id, reply.From.Id, DateTime.Now, true, true, true, true);
                    }
                }

                //Commands for AdCleaner admins
                if (reply != null && admins.Any(a => a.Id == message.From.Id) && isMeAdmin && message.Text != null)
                {
                    if (message.Text == "/kill")
                    {
                        if (reply.Type != MessageType.ServiceMessage)
                        {
                            string[] words = null;
                            if (reply.Text != null)
                                words = reply.Text.Split();
                            else if (reply.Caption != null)
                                words = reply.Caption.Split();

                            if (words != null)
                            {
                                foreach (var word in words)
                                {
                                    if (word.StartsWith("@"))
                                    {
                                        if (!keywords.Any(a => a.Keyword == word.ToLower() && a.Type == Models.Type.Group))
                                            Database.KeywordsAdd(word.ToLower(), Models.Type.Group);
                                    }
                                }
                                foreach (var ent in reply.Entities)
                                {
                                    if (ent.Url != null && (ent.Url.StartsWith("http://") || ent.Url.StartsWith("https://")))
                                    {
                                        if (!keywords.Any(a => a.Keyword == ent.Url.ToString().ToLower() && a.Type == Models.Type.Group))
                                            Database.KeywordsAdd(ent.Url.ToString().ToLower(), Models.Type.Group);
                                    }
                                }
                            }
                        }
                        try
                        {
                            await Bot.DeleteMessageAsync(message.Chat.Id, reply.MessageId);
                            await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                        }
                        catch (Telegram.Bot.Exceptions.ApiRequestException) { }
                        Database.UserAdd(reply.From, Models.Type.Spamer);
                    }
                    else if (message.Text == "/burn")
                    {
                        try
                        {
                            await Bot.DeleteMessageAsync(message.Chat.Id, reply.MessageId);
                            await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                        }
                        catch (Telegram.Bot.Exceptions.ApiRequestException) { }
                        Database.UserAdd(reply.From, Models.Type.Spamer);
                    }
                    else if (message.Text.StartsWith("/mute"))
                    {
                        try
                        {
                            int time = 0;
                            var words = message.Text.Split();
                            if (words.Length >= 2)
                            {
                                try
                                {
                                    time = Int32.Parse(words[1]);
                                }
                                catch (FormatException) { }
                            }
                            else
                                time = 1;

                            var date = DateTime.Now.AddMinutes(time);
                            await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                            await Bot.RestrictChatMemberAsync(message.Chat.Id, reply.From.Id, date, false, false, false, false);
                            string name = reply.From.Username != null ? $"@{reply.From.Username}" : $"[{reply.From.FirstName}](tg://user?id={reply.From.Id})";
                            string msg = $"Уважаемый(ая) {name}!\nВам нужно посидеть в readonly {time} минут(ы).";
                            await Bot.SendTextMessageAsync(message.Chat.Id, msg, ParseMode.Markdown);
                        }
                        catch (Exception ex)
                        {
                            string result = $"{ex.Message}\nFROM:{message.Chat.FirstName}\nCOMMAND:/mute\nTO:{reply.From.FirstName} {reply.From.LastName}";
                            await Bot.SendTextMessageAsync(448868194, result);
                        }
                    }
                    else if (message.Text.StartsWith("/zaebal"))
                    {
                        try
                        {
                            int time = 0;
                            var words = message.Text.Split();
                            if (words.Length >= 2)
                            {
                                try
                                {
                                    time = Int32.Parse(words[1]);
                                }
                                catch (FormatException) { }
                            }
                            else
                                time = 30;

                            var date = DateTime.Now.AddMinutes(time);
                            await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                            await Bot.RestrictChatMemberAsync(message.Chat.Id, reply.From.Id, date, true, false, false, false);

                            string name = reply.From.Username != null ? $"@{reply.From.Username}" : $"[{reply.From.FirstName}](tg://user?id={reply.From.Id})";
                            string msg = $"Уважаемый(ая) {name}!\nВы заебали кидать стикеры/картинки не в тему, подумайте над своим поведением {time} минут(ы)!";
                            await Bot.SendTextMessageAsync(message.Chat.Id, msg, ParseMode.Markdown);
                        }
                        catch (Exception ex)
                        {
                            string result = $"{ex.Message}\nFROM:{message.Chat.FirstName}\nCOMMAND:/zaebal\nTO:{reply.From.FirstName} {reply.From.LastName}";
                            await Bot.SendTextMessageAsync(448868194, result);
                        }
                    }
                    else if (message.Text == "/unmute")
                    {
                        await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                        await Bot.RestrictChatMemberAsync(message.Chat.Id, reply.From.Id, DateTime.Now, true, true, true, true);
                    }
                    else if (message.Text == "/clean")
                    {
                        try
                        {
                            await Bot.DeleteMessageAsync(message.Chat.Id, reply.MessageId);
                            await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                        }
                        catch (Telegram.Bot.Exceptions.ApiRequestException) { }
                    }
                }

                //Keywords check
                if (message.Text != null && keywords.Any(a => message.Text.ToLower().Contains(a.Keyword) && a.Type == Models.Type.Group) || message.Caption != null && keywords.Any(a => message.Caption.ToLower().Contains(a.Keyword) && a.Type == Models.Type.Group))
                {
                    Database.UserAdd(message.From, Models.Type.Spamer);
                    try
                    {
                        await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                    }
                    catch (Telegram.Bot.Exceptions.ApiRequestException) { }
                }

                //Forwards check
                if (message.ForwardFromChat != null && keywords.Any(a => message.ForwardFromChat.Title.ToLower().Contains(a.Keyword) && a.Type == Models.Type.Forward))
                {
                    Database.ChatAdd(message.ForwardFromChat, Models.Type.SpamerChat);
                    Database.UserAdd(message.From, Models.Type.Spamer);
                    try
                    {
                        await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                    }
                    catch (Telegram.Bot.Exceptions.ApiRequestException) { }
                }

                //Wanted check
                if (message.From.Username != null && keywords.Any(a => a.Keyword == message.From.Username.ToLower() && a.Type == Models.Type.Wanted))
                {
                    Database.UserAdd(message.From, Models.Type.Spamer);
                    try
                    {
                        await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                    }
                    catch (Telegram.Bot.Exceptions.ApiRequestException) { }
                }

                spamers = context.Users.Where(a => a.Type == Models.Type.Spamer);

                //Butler
                if (message.NewChatMembers != null)
                {
                    foreach (var newUser in message.NewChatMembers)
                    {
                        if (spamers.Any(a => a.Id == newUser.Id) || newUser.Username != null && keywords.Any(a => a.Keyword == newUser.Username.ToLower() && a.Type == Models.Type.Wanted))
                        {
                            ChatMember[] adminArr = await Bot.GetChatAdministratorsAsync(message.Chat.Id);
                            if (adminArr.Any(a => a.User.Id == me.Id))
                            {
                                await Bot.KickChatMemberAsync(message.Chat.Id, newUser.Id);
                                string file = $@"{Directory.GetCurrentDirectory()}/Resources/ButlerSticker.webp";
                                using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                                {
                                    var fts = new FileToSend(file, fileStream);
                                    await Bot.SendStickerAsync(message.Chat.Id, fts, false, message.MessageId);
                                }
                            }
                            else
                            {
                                Random rand = new Random();
                                int temp = rand.Next(adminArr.Length);
                                var otheradmin = adminArr[temp].User;

                                var admin = otheradmin.Username != null ? $"@{otheradmin.Username}" : $"[{otheradmin.FirstName}](tg://user?id={otheradmin.Id})";
                                var spamer = newUser.Username != null ? $"@{newUser.Username}" : $"[{newUser.FirstName}](tg://user?id={newUser.Id})";

                                var msg = $"{admin}!Бан упыря!!!===>{spamer}<===";

                                await Bot.SendTextMessageAsync(message.Chat.Id, msg);
                            }
                        }
                    }
                }

                //spamers work
                if (spamers.Any(a => a.Id == message.From.Id))
                {
                    try
                    {
                        ChatMember[] adminArr = await Bot.GetChatAdministratorsAsync(message.Chat.Id);
                        if (adminArr.Any(a => a.User.Id == me.Id))
                        {
                            await Bot.KickChatMemberAsync(message.Chat.Id, message.From.Id);
                        }
                        else
                        {
                            Random rand = new Random();
                            int temp = rand.Next(adminArr.Length);
                            var otheradmin = adminArr[temp].User;

                            var admin = otheradmin.Username != null ? $"@{otheradmin.Username}" : $"[{otheradmin.FirstName}](tg://user?id={otheradmin.Id})";
                            var spamer = message.From.Username != null ? $"@{message.From.Username}" : $"[{message.From.FirstName}](tg://user?id={message.From.Id})";

                            var msg = $"{admin}!Бан упыря!!!===>{spamer}<===";

                            await Bot.SendTextMessageAsync(message.Chat.Id, msg, ParseMode.Markdown);
                        }
                    }
                    catch (Telegram.Bot.Exceptions.ApiRequestException e) { }
                }
                if (reply != null && spamers.Any(a => a.Id == reply.From.Id))
                {
                    try
                    {
                        ChatMember[] adminArr = await Bot.GetChatAdministratorsAsync(message.Chat.Id);
                        if (adminArr.Any(a => a.User.Id == me.Id))
                        {
                            await Bot.KickChatMemberAsync(message.Chat.Id, reply.From.Id);
                        }
                        else
                        {
                            Random rand = new Random();
                            int temp = rand.Next(adminArr.Length);
                            var otheradmin = adminArr[temp].User;

                            var admin = otheradmin.Username != null ? $"@{otheradmin.Username}" : $"[{otheradmin.FirstName}](tg://user?id={otheradmin.Id})";
                            var spamer = reply.From.Username != null ? $"@{reply.From.Username}" : $"[{reply.From.FirstName}](tg://user?id={reply.From.Id})";

                            var msg = $"{admin}!Бан Упрыя!!!===>{spamer}<===";

                            await Bot.SendTextMessageAsync(message.Chat.Id, msg, ParseMode.Markdown);
                        }
                    }
                    catch (Telegram.Bot.Exceptions.ApiRequestException e) { }
                }
            }
        }
        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var context = new DBcontext();
            var me = await Bot.GetMeAsync();

            var keyboard = new InlineKeyboardMarkup(new[]
            {
                    new []
                    {
                        new InlineKeyboardCallbackButton("Сообщения", "Сообщения"),
                        new InlineKeyboardCallbackButton("Группы", "Группы"),
                    },
                    new []
                    {
                        new InlineKeyboardCallbackButton("Доска почета", "Доска почета"),
                        new InlineKeyboardCallbackButton("Освободить", "Освободить"),
                    }
            });

            var callback = callbackQueryEventArgs.CallbackQuery;
            //Message from bot
            var message = callbackQueryEventArgs.CallbackQuery.Message;

            if (callback.Data == "Сообщения")
            {
                if (!messages.Any(a => a == message.Chat.Id))
                    messages.Add(message.Chat.Id);
                string usage = $"Настройка ключевых слов для сообщений.\nЕсли в тексте сообщения содержится фраза или слово из списка -пользователь будет наказан.";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись", "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, usage, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Добавить" && messages.Any(a => a == message.Chat.Id))
            {
                var k = new Keyboard();
                k.Add("Закончить");
                if (!kwdAddMode.Contains(message.Chat.Id))
                    kwdAddMode.Add(message.Chat.Id);
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, "Режим ввода ключевых слов для сообщений\nВведите слово или фразу:", replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Закончить" && message.Text.Contains("Режим ввода ключевых слов для сообщений"))
            {
                string usage = $"Настройка ключевых слов для сообщений.\nЕсли в тексте сообщения содержится фраза или слово из списка - пользователь будет наказан.";

                kwdAddMode.Remove(message.Chat.Id);
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись", "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, usage, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Показать" && messages.Any(a => a == message.Chat.Id))
            {
                var keywords = context.Keywords.Where(a => a.Type == Models.Type.Group);
                string readfile = "";
                foreach (var a in keywords)
                {
                    readfile += $"{a.Keyword}\n";
                }
                if (string.IsNullOrWhiteSpace(readfile))
                    readfile = "Cписок пуст.";
                string usage = $"Настройка ключевых слов для сообщений.\nЕсли в тексте сообщения содержится фраза или слово из списка - пользователь будет наказан.\n`{readfile}`";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись", "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, usage, ParseMode.Markdown, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Удалить запись" && messages.Any(a => a == message.Chat.Id))
            {
                var keywords = context.Keywords.Where(a => a.Type == Models.Type.Group);
                string readfile = "";
                foreach (var a in keywords)
                {
                    readfile += $"{a.Keyword}\n";
                }
                if (string.IsNullOrWhiteSpace(readfile))
                    readfile = "Cписок пуст";
                string usage = $"Режим удаления ключевых слов для сообщений.\nВведите слово или фразу. Которое нужно удалить.\n`{readfile}`";
                var k = new Keyboard();
                k.Add("Закончить");
                if (!kwdRemoveMode.Contains(message.Chat.Id))
                    kwdRemoveMode.Add(message.Chat.Id);
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, usage, ParseMode.Markdown, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Закончить" && message.Text.Contains("Введите слово или фразу."))
            {
                kwdRemoveMode.Remove(message.Chat.Id);
                string usage = $"Настройка ключевых слов для сообщений.\nЕсли в тексте сообщения содержится фраза или слово из списка - пользователь будет наказан.";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись", "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, usage, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }

            else if (callback.Data == "Группы")
            {
                if (!groups.Any(a => a == message.Chat.Id))
                    groups.Add(message.Chat.Id);
                string usage = $"Настройка ключевых слов для групп.\nЕсли сообщение переслано из канала или чата из списка - пользователь будет наказан.";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись", "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(callback.Message.Chat.Id, callback.Message.MessageId, usage, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Добавить" && groups.Any(a => a == message.Chat.Id))
            {
                var k = new Keyboard();
                k.Add("Закончить");
                if (!gKwdAddMode.Contains(message.Chat.Id))
                    gKwdAddMode.Add(message.Chat.Id);
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, "Режим ввода ключевых слов для групп\nВведите название чата или канала:", replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }

            }
            else if (callback.Data == "Закончить" && message.Text.Contains("Режим ввода ключевых слов для групп"))
            {
                string usage = $"Настройка ключевых слов для групп.\nЕсли сообщение переслано из канала или чата из списка - пользователь будет наказан.";
                gKwdAddMode.Remove(message.Chat.Id);
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись", "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(callback.Message.Chat.Id, callback.Message.MessageId, usage, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Показать" && groups.Any(a => a == message.Chat.Id))
            {
                var keywords = context.Keywords.Where(a => a.Type == Models.Type.Forward);
                string readfile = "";
                foreach (var a in keywords)
                {
                    readfile += $"{a.Keyword}\n";
                }
                if (string.IsNullOrWhiteSpace(readfile))
                    readfile = "Cписок пуст.";
                string usage = $"Настройка ключевых слов для групп.\nЕсли сообщение переслано из канала или чата из списка - пользователь будет наказан.\n`{readfile}`";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись", "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, usage, ParseMode.Markdown, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Удалить запись" && groups.Any(a => a == message.Chat.Id))
            {
                var keywords = context.Keywords.Where(a => a.Type == Models.Type.Forward);
                string readfile = "";
                foreach (var a in keywords)
                {
                    readfile += $"{a.Keyword}\n";
                }
                if (string.IsNullOrWhiteSpace(readfile))
                    readfile = "Cписок пуст.";
                string usage = $"Режим удаления ключевых слов для групп\nВведите название группы. Которое нужно удалить\n`{readfile}`";
                var k = new Keyboard();
                k.Add("Закончить");
                if (!gKwdRemoveMode.Contains(message.Chat.Id))
                    gKwdRemoveMode.Add(message.Chat.Id);
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, usage, ParseMode.Markdown, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }

            }
            else if (callback.Data == "Закончить" && message.Text.Contains("Введите название группы."))
            {
                gKwdRemoveMode.Remove(message.Chat.Id);
                string usage = $"Настройка ключевых слов для групп.\nЕсли сообщение переслано из канала или чата из списка - пользователь будет наказан.";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись",   "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, usage, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }

            else if (callback.Data == "Доска почета")
            {
                if (!deck.Any(a => a == message.Chat.Id))
                    deck.Add(message.Chat.Id);
                string usage = $"Доска почета.\nЕсли юзернейм нового участника группы будет совпадать с доской почета - участник будет наказан.Так же банхаммер сверит с ней всех участников всех чатов.";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись",   "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(callback.Message.Chat.Id, callback.Message.MessageId, usage, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Добавить" && deck.Any(a => a == message.Chat.Id))
            {
                var k = new Keyboard();
                k.Add("Закончить");
                if (!wantedAddMode.Contains(message.Chat.Id))
                    wantedAddMode.Add(message.Chat.Id);
                try
                {
                    await Bot.EditMessageTextAsync(callback.Message.Chat.Id, callback.Message.MessageId, "Режим ввода ориентировок\nВведите username:", replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException e) { }
            }
            else if (callback.Data == "Закончить" && message.Text.Contains("Режим ввода ориентировок"))
            {
                wantedAddMode.Remove(message.Chat.Id);
                string usage = $"Доска почета.\nЕсли юзернейм нового участника группы будет совпадать с доской почета - участник будет наказан.Так же банхаммер сверит с ней всех участников всех чатов.";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись",   "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(callback.Message.Chat.Id, callback.Message.MessageId, usage, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Показать" && deck.Any(a => a == message.Chat.Id))
            {
                var keywords = context.Keywords.Where(a => a.Type == Models.Type.Wanted);
                string readfile = "";
                foreach (var a in keywords)
                {
                    readfile += $"{a.Keyword}\n";
                }
                if (string.IsNullOrWhiteSpace(readfile))
                    readfile = "Cписок пуст.";
                string usage = $"Доска почета.\nЕсли юзернейм нового участника группы будет совпадать с доской почета - участник будет наказан.Так же банхаммер сверит с ней всех участников всех чатов.\n`{readfile}`";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись",   "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(callback.Message.Chat.Id, callback.Message.MessageId, usage, ParseMode.Markdown, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Удалить запись" && deck.Any(a => a == message.Chat.Id))
            {
                var keywords = context.Keywords.Where(a => a.Type == Models.Type.Wanted);
                string readfile = "";
                foreach (var a in keywords)
                {
                    readfile += $"{a.Keyword}\n";
                }
                if (string.IsNullOrWhiteSpace(readfile))
                    readfile = "Cписок пуст.";
                string usage = $"Введите username. Которое нужно удалить.\n`{readfile}`";
                var k = new Keyboard();
                k.Add("Закончить");
                if (!wantedRemoveMode.Contains(message.Chat.Id))
                    wantedRemoveMode.Add(message.Chat.Id);
                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, usage, ParseMode.Markdown, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }

            }
            else if (callback.Data == "Закончить" && message.Text.Contains("Введите username."))
            {
                wantedRemoveMode.Remove(message.Chat.Id);
                string usage = $"Доска почета.\nЕсли юзернейм нового участника группы будет совпадать с доской почета - участник будет наказан.Так же банхаммер сверит с ней всех участников всех чатов.";
                var k = new Keyboard();
                k.Add("Добавить", "Показать", "Удалить запись",   "Назад");
                try
                {
                    await Bot.EditMessageTextAsync(callback.Message.Chat.Id, callback.Message.MessageId, usage, replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }

            else if (callback.Data == "Освободить")
            {
                if (!unban.Any(a => a == message.Chat.Id))
                    unban.Add(message.Chat.Id);
                var k = new Keyboard();
                k.Add("Закончить");
                try
                {
                    await Bot.EditMessageTextAsync(callback.Message.Chat.Id, callback.Message.MessageId, "Введите username который нужно освободить:", replyMarkup: k.key);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }
            else if (callback.Data == "Закончить" && unban.Any(a => a == message.Chat.Id))
            {
                unban.Remove(message.Chat.Id);
                try
                {
                    await Bot.EditMessageTextAsync(callback.Message.Chat.Id, callback.Message.MessageId, $"Настройки {me.Username}", replyMarkup: keyboard);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }

            else if (callback.Data == "Назад")
            {
                if (messages.Any(a => a == message.Chat.Id)) messages.Remove(message.Chat.Id);
                if (groups.Any(a => a == message.Chat.Id)) groups.Remove(message.Chat.Id);
                if (deck.Any(a => a == message.Chat.Id)) deck.Remove(message.Chat.Id);
                if (unban.Any(a => a == message.Chat.Id)) unban.Remove(message.Chat.Id);

                try
                {
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, $"Настроки {me.Username}", replyMarkup: keyboard);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }

        }
        private static async void BotOnUpdateRecieved(object sender, UpdateEventArgs updateEventArgs)
        {
            var post = updateEventArgs.Update.ChannelPost;
            var me = Bot.GetMeAsync();

            if (post == null) return;

            await Bot.SendTextMessageAsync(post.Chat.Id, post.MessageId.ToString());
        }
        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            var error = receiveErrorEventArgs.ApiRequestException;
            Console.WriteLine(error.Message);
        }
    }
}

