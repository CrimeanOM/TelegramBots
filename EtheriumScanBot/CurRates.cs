﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Globalization;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace EtheriumScanBot
{
    public class ETHAddr
    {
        public string status { get; set; }
        public string message { get; set; }
        public decimal result { get; set; }
    }

    public class Coinbase
    {
        public Data data { get; set; }
    }
    public class Data
    {
        public string currency { get; set; }
        public CoinbaseRates rates { get; set; }
    }
    public class CoinbaseRates
    {
        public string BTC { get; set; }
        public string ETH { get; set; }
        public string EUR { get; set; }
        public string RUB { get; set; }
        public string USD { get; set; }
    }

    public class Poloniex
    {
        public USDTETH USDT_ETH { get; set; }
    }
    public class USDTETH
    {
        public string last { get; set; }
    }

    public class Kraken
    {
        public List<object> error { get; set; }
        public Result result { get; set; }
    }
    public class Result
    {
        public XETHZUSD XETHZUSD { get; set; }
    }
    public class XETHZUSD
    {
        public List<string> c { get; set; }
    }

    public class BTCe
    {
        public EthUsd eth_usd { get; set; }
        public EthRur eth_rur { get; set; }
    }
    public class EthUsd
    {
        public double last { get; set; }
    }
    public class EthRur
    {
        public double last { get; set; }
    }

    public class Curr
    {
        public static readonly string token = @"12345"; // etherscan token

        public static async Task<string> GetBalance(string addr, string source = "Coinbase")
        {
            string result = "", ethReq = "";
            decimal ethBalance = 0;
            decimal wei = Convert.ToDecimal(Math.Pow(10, 18));
            var client = new HttpClient();
            try
            {
                ethReq = await client.GetStringAsync($@"https://api.etherscan.io/api?module=account&action=balance&address={addr}&tag=latest&apikey={token}");
                ETHAddr ethRes = JsonConvert.DeserializeObject<ETHAddr>(ethReq);
                ethBalance = ethRes.result / wei;
            }
            catch (Exception)
            {
                result = "Etherscan не прислал данные :(\nПопробуйте позже";
            }

            CultureInfo CurrentCulture = new CultureInfo("en-US");
            switch (source)
            {
                case "Coinbase":
                    try
                    {
                        decimal usd, rur;
                        string reqCoinbase = await client.GetStringAsync($@"https://api.coinbase.com/v2/exchange-rates?currency=ETH");
                        Coinbase resCoinbase = JsonConvert.DeserializeObject<Coinbase>(reqCoinbase);
                        usd = ethBalance * Convert.ToDecimal(resCoinbase.data.rates.USD);
                        rur = ethBalance * Convert.ToDecimal(resCoinbase.data.rates.RUB);
                        result = $"Баланс адреса:\nETH: {ethBalance} Ξ\nUSD: {usd:#,#.####} $\nRUR: {rur:#,#.##} ₽";
                    }
                    catch (WebException)
                    {
                        result = "Coinbase не прислал данные:(\nПопробуйте позже";
                    }
                    break;
                case "Poloniex":
                    try
                    {
                        decimal usdt;
                        string requestPolo = await client.GetStringAsync($@"https://poloniex.com/public?command=returnTicker");
                        Poloniex response = JsonConvert.DeserializeObject<Poloniex>(requestPolo);
                        usdt = ethBalance * Convert.ToDecimal(response.USDT_ETH.last);
                        result = $"Баланс адреса:\nETH: {ethBalance} Ξ\nUSDT: {usdt:#,#.####} $";
                    }
                    catch (WebException)
                    {
                        result = "Poloniex не прислал данные:(\nПопробуйте позже";
                    }
                    break;
                case "Kraken":
                    try
                    {
                        decimal usd;
                        string requestKraken = await client.GetStringAsync($@"https://api.kraken.com/0/public/Ticker?pair=XETHZUSD");
                        Kraken response = JsonConvert.DeserializeObject<Kraken>(requestKraken);
                        usd = ethBalance * Convert.ToDecimal(response.result.XETHZUSD.c[0]);
                        result = $"Баланс адреса:\nETH: {ethBalance} Ξ\nUSD: {usd:#,#.####} $";
                    }
                    catch (Exception)
                    {
                        result = "Kraken не прислал данные :(";
                    }
                    break;
                case "WEX":
                    try
                    {
                        decimal usd, rur;
                        string requestBTC = await client.GetStringAsync("https://WEX.nz/api/3/ticker/eth_usd-eth_rur");
                        BTCe jsonRespBTC = JsonConvert.DeserializeObject<BTCe>(requestBTC);
                        usd = ethBalance * Convert.ToDecimal(jsonRespBTC.eth_usd.last);
                        rur = ethBalance * Convert.ToDecimal(jsonRespBTC.eth_rur.last);
                        result = $"Баланс адреса:\nETH: {ethBalance} Ξ\nUSD: {usd:#,#.####} $\nRUR: {rur:#,#.##} ₽";
                    }
                    catch (Exception)
                    {
                        result = "WEX не прислал данные :(";
                    }
                    break;
            }
            return result;
        }
    }
}
