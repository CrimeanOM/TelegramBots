﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Globalization;

using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;


namespace EtheriumScanBot
{
    class Program
    {
        public static readonly TelegramBotClient Bot = new TelegramBotClient("TelegramToken");
        public static readonly string token = @"1234"; // etherscan token
        public static string path = $@"{Directory.GetCurrentDirectory()}\Resources\";
        public static List<long> addrAddMode = new List<long>();
        public static List<long> addrRemoveList = new List<long>();

        static void Main(string[] args)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
                dirInfo.Create();

            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnUpdate += BotOnUpdateRecieved;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            Bot.StartReceiving();

            while (true)
            {
                Thread.Sleep(500);
            }
        }
        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            Account acc = new Account();

            var me = Bot.GetMeAsync().Result;
            var message = messageEventArgs.Message;

            if (message == null || message.Text == null) return;

            if (message.Chat.Type == ChatType.Private)
            {
                //получение объекта из файла по id
                string file = $@"{message.From.Id}";
                if (System.IO.File.Exists(file))
                {
                    string lines = System.IO.File.ReadAllText($@"{message.From.Id}");
                    acc = JsonConvert.DeserializeObject<Account>(lines);
                }
                else
                {
                    acc = new Account(message.From);
                    System.IO.File.WriteAllText($@"{acc.Id}", JsonConvert.SerializeObject(acc));
                    string lines = System.IO.File.ReadAllText($@"{message.From.Id}");
                    acc = JsonConvert.DeserializeObject<Account>(lines);
                }

                foreach (var id in addrAddMode)
                {
                    if (id == message.From.Id)
                    {
                        string[] words = message.Text.Split('-');
                        if (words.Length == 2 && words[0].StartsWith("0x"))
                        {
                            Wallet wallet = new Wallet();
                            wallet.Addr = words[0].TrimEnd(' ');
                            wallet.Name = words[1].TrimStart(' ');
                            acc.Wallets.Add(wallet);
                            System.IO.File.WriteAllText($@"{acc.Id}", JsonConvert.SerializeObject(acc));
                            string usage = $"Добавлен новый адрес {wallet.Addr} с именем {wallet.Name}";
                            await Bot.SendTextMessageAsync(message.Chat.Id, usage);
                        }
                        else if (words.Length == 2)
                        {
                            string msg = $"Неверный формат адреса!\n";
                            await Bot.SendTextMessageAsync(message.Chat.Id, msg, ParseMode.Markdown);
                        }
                    }
                }
                foreach (var id in addrRemoveList)
                {
                    if (id == message.From.Id && message.Text != "Удалить")
                    {
                        if (acc.Wallets.Any(a => a.Name == message.Text))
                        {
                            var wal = from a in acc.Wallets where a.Name == message.Text select a;
                            acc.Wallets.Remove(wal.Single());
                            System.IO.File.WriteAllText($@"{acc.Id}", JsonConvert.SerializeObject(acc));
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Адрес удален!");
                        }
                        else
                            await Bot.SendTextMessageAsync(message.Chat.Id, "У Вас нет адреса с таким именем!");
                    }
                }

                List<string> btnz = new List<string> { "Настройки", "Мои адреса", "Создать", "Удалить" };


                if (acc.Wallets.Count == 0 && !btnz.Contains(message.Text))
                    Custom_keyboard.Add("Настройки", message.Chat.Id, "Вам нужно добавить адрес\nДля добавления адреса перейдите в Настройки => Мои адреса => Создать");

                SettingsMenu(message, acc);
                MainMenu(message, acc);

                if (message.Text == "/start" && acc.Wallets.Count != 0)
                {
                    Custom_keyboard.Add("Баланс", "Курс", "Настройки", message.Chat.Id, "Выберите");
                }
                else if (message.Text == "/help")
                {
                    string msg = $@"Для просмотра баланса нужного Вам адреса - просто отправьте адрес мне.
/start - для меню.
/dao - баланс DAO.
/help - список команд.
/donate - помочь боту.
";
                    await Bot.SendTextMessageAsync(message.Chat.Id, msg);
                }
                else if (message.Text.StartsWith("0x"))
                {
                    string[] words = message.Text.Split(' ');
                    var msg = Curr.GetBalance(words[0]);
                    await Bot.SendTextMessageAsync(message.Chat.Id, msg.Result);
                }
            }

            if (message.Text == "/dao")
            {
                string token = @"12345"; //etherscan token
                string addr = $@"0xA0cAf066448ec9ee5B6D2d0c82Fa497953044312";

                string result = "", ethReq = "";
                decimal ethBalance = 0;
                decimal wei = Convert.ToDecimal(Math.Pow(10, 18));
                var client = new HttpClient();
                try
                {
                    ethReq = await client.GetStringAsync($@"https://api.etherscan.io/api?module=account&action=balance&address={addr}&tag=latest&apikey={token}");
                    ETHAddr ethRes = JsonConvert.DeserializeObject<ETHAddr>(ethReq);
                    ethBalance = ethRes.result / wei;
                    decimal usd, rur;
                    string reqCoinbase = await client.GetStringAsync($@"https://api.coinbase.com/v2/exchange-rates?currency=ETH");
                    Coinbase resCoinbase = JsonConvert.DeserializeObject<Coinbase>(reqCoinbase);
                    usd = ethBalance * Convert.ToDecimal(resCoinbase.data.rates.USD);
                    rur = ethBalance * Convert.ToDecimal(resCoinbase.data.rates.RUB);
                    result = $"Баланс адреса:\nETH: {ethBalance} Ξ\nUSD: {usd:#,#.####} $\nRUR: {rur:#,#.##} ₽";
                }
                catch (WebException)
                {
                    result = "Что-то пошло не так:(\nПопробуйте позже";
                }
                await Bot.SendTextMessageAsync(message.Chat.Id, result, ParseMode.Markdown);
            }
        }
        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callback = callbackQueryEventArgs.CallbackQuery;
            var message = callback.Message;
            var me = await Bot.GetMeAsync();

            string file = $@"{callback.From.Id}";

            string lines = System.IO.File.ReadAllText($@"{callback.From.Id}");
            Account acc = JsonConvert.DeserializeObject<Account>(lines);

            if (callback.Data.StartsWith("0x"))
            {
                try
                {
                    var k = acc.GetKeyboard();
                    await Bot.AnswerCallbackQueryAsync(callback.Id, $"Проверяю {callback.Data}");
                    var msg = Curr.GetBalance(callback.Data, acc.Source);
                    await Bot.EditMessageTextAsync(message.Chat.Id, message.MessageId, msg.Result, ParseMode.Markdown, replyMarkup: k);
                }
                catch (Telegram.Bot.Exceptions.ApiRequestException) { }
            }

        }
        private static async void BotOnUpdateRecieved(object sender, UpdateEventArgs updateEventArgs)
        {
            var post = updateEventArgs.Update.ChannelPost;
            var me = Bot.GetMeAsync();

            if (post == null) return;

            string usage = $"Увы, но канал: {post.Chat.Title} собирает ботов в качестве мертвых душ :(\nDAO Crypto Zen - Для тех кто хочет увеличить свои вложения. Подробности тут: http://telegra.ph/Manifest-o-servise-06-01";

            await Bot.SendTextMessageAsync(post.Chat.Id, usage);
        }

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            var error = receiveErrorEventArgs.ApiRequestException;
            Console.WriteLine(error.Message);
        }

        public static void SettingsMenu(Message message, Account acc)
        {
            if (message.Text == "Настройки")
            {
                Custom_keyboard.Add("Мои адреса", "Источники курса", "Назад", message.Chat.Id, "Выберите");
            }
            else if (message.Text == "Мои адреса")
            {
                string msg = acc.GetAddresses();
                Custom_keyboard.Add("Создать", "Удалить", "Назад", message.Chat.Id, msg);
            }
            else if (message.Text == "Создать")
            {
                string msg = $"Введите адрес и его название через тире\nПример:\n `0x141cA50EAE44dDBbcfFc9E7A7E1ADe2DAf46a0Fd - Личный кошелек.`";
                if (!addrAddMode.Contains(message.From.Id))
                    addrAddMode.Add(message.From.Id);
                Custom_keyboard.Add("Закончить", message.Chat.Id, msg);
            }
            else if (message.Text == "Удалить")
            {
                string msg = acc.GetAddresses();
                string usage = $"Введите Название адреса который хотите удалить\n{msg}";
                if (!addrRemoveList.Contains(message.From.Id))
                    addrRemoveList.Add(message.From.Id);
                Custom_keyboard.Add("Закончить", message.Chat.Id, usage);
            }
            else if (message.Text == "Закончить")
            {
                if (addrAddMode.Contains(message.Chat.Id))
                    addrAddMode.Remove(message.Chat.Id);
                if (addrRemoveList.Contains(message.Chat.Id))
                    addrRemoveList.Remove(message.Chat.Id);

                if (acc.Wallets != null)
                    Custom_keyboard.Add("Мои адреса", "Источники курса", "Назад", message.Chat.Id, "Выберите");
                else
                    Custom_keyboard.Add("Настройки", message.Chat.Id, "Выберите");
            }
            else if (message.Text == "Источники курса")
            {
                Custom_keyboard.Add("Coinbase", "Poloniex", "Kraken", "WEX", "Назад", message.Chat.Id, "Откуда будем брать курс?");
            }
            else if (message.Text == "Coinbase" || message.Text == "Poloniex" || message.Text == "Kraken" || message.Text == "WEX")
            {
                acc.Source = message.Text;
                System.IO.File.WriteAllText($@"{acc.Id}", JsonConvert.SerializeObject(acc));
                string usage = $"Теперь я буду брать курс с {acc.Source}";
                Custom_keyboard.Add("Мои адреса", "Источники курса", "Назад", message.Chat.Id, usage);
            }
        }
        public async static void MainMenu(Message message, Account acc)
        {
            if (acc.Wallets.Count != 0)
            {
                if (message.Text == "Баланс")
                {
                    var k = acc.GetKeyboard();
                    await Bot.SendTextMessageAsync(message.Chat.Id, "Ваши адреса:", replyMarkup: k);
                }
                else if (message.Text == "Курс")
                {
                    CultureInfo CurrentCulture = new CultureInfo("en-US");

                    var client = new HttpClient();
                    string coinbase, kraken, polo, btce;
                    try
                    {
                        decimal cUsd, cRur;
                        string reqCoinbase = await client.GetStringAsync("https://api.coinbase.com/v2/exchange-rates?currency=ETH");
                        Coinbase resCoinbase = JsonConvert.DeserializeObject<Coinbase>(reqCoinbase);
                        cUsd = Convert.ToDecimal(resCoinbase.data.rates.USD);
                        cRur = Convert.ToDecimal(resCoinbase.data.rates.RUB);
                        coinbase = $"Coinbase\nETH: {cUsd:#,#.####} $\nETH: {cRur:#,#.##} ₽\n\n";
                    }
                    catch (Exception e)
                    {
                        coinbase = $"Coinbase не прислал данные :(\n\n";
                    }
                    try
                    {
                        decimal kUsd;
                        string reqKraken = await client.GetStringAsync($@"https://api.kraken.com/0/public/Ticker?pair=XETHZUSD");
                        Kraken resKraken = JsonConvert.DeserializeObject<Kraken>(reqKraken);
                        kUsd = Convert.ToDecimal(resKraken.result.XETHZUSD.c[0]);
                        kraken = $"Kraken\nETH: {kUsd:#,#.####} $\n\n";
                    }
                    catch (Exception)
                    {
                        kraken = $"Kraken не прислал данные :(\n\n";
                    }
                    try
                    {
                        decimal usdt;
                        string reqPolo = await client.GetStringAsync($@"https://poloniex.com/public?command=returnTicker");
                        Poloniex resPolo = JsonConvert.DeserializeObject<Poloniex>(reqPolo);
                        usdt = Convert.ToDecimal(resPolo.USDT_ETH.last);
                        polo = $"Polo\nETH: {usdt:#,#.####} USDT\n\n";
                    }
                    catch (Exception)
                    {
                        polo = $"Poloniex не прислал данные :(\n\n";
                    }
                    try
                    {
                        decimal bUsd, bRur;
                        string requestBTC = await client.GetStringAsync("https://WEX.nz/api/3/ticker/eth_usd-eth_rur");
                        BTCe jsonRespBTC = JsonConvert.DeserializeObject<BTCe>(requestBTC);
                        bUsd = Convert.ToDecimal(jsonRespBTC.eth_usd.last);
                        bRur = Convert.ToDecimal(jsonRespBTC.eth_rur.last);
                        btce = $"WEX\nETH: {bUsd:#,#.####} $\nETH: {bRur:#,#.##} ₽\n\n";
                    }
                    catch (Exception)
                    {
                        btce = $"WEX не прислал данные :(\n";
                    }
                    string result = coinbase + kraken + polo + btce;

                    await Bot.SendTextMessageAsync(message.Chat.Id, result);
                }
                else if (message.Text == "Назад")
                {
                    Custom_keyboard.Add("Баланс", "Курс", "Настройки", message.Chat.Id, "Выберите");
                }
            }
        }
    }
}
