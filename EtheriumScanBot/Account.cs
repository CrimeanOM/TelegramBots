﻿using System.Collections.Generic;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace EtheriumScanBot
{
    class Account
    {
        public ChatId Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string LanguageCode { get; set; }
        public List<Wallet> Wallets { get; set; }
        public string Source { get; set; }

        public Account() { }

        public Account(User user)
        {
            this.Id = user.Id;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Username = user.Username;
            this.LanguageCode = user.LanguageCode;
            this.Wallets = new List<Wallet>();
            this.Source = "Coinbase";
        }

        public InlineKeyboardMarkup GetKeyboard()
        {
            List<InlineKeyboardButton[]> rows = new List<InlineKeyboardButton[]>();
            List<InlineKeyboardButton> items = new List<InlineKeyboardButton>();

            foreach (var wallet in Wallets)
            {
                InlineKeyboardButton btn = new InlineKeyboardButton(wallet.Name, wallet.Addr);
                items.Add(btn);
            }

            foreach (var item in items)
            {
                rows.Add(new[] { item, });
            }

            InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup(rows.ToArray());

            return keyboard;
        }

        public string GetAddresses()
        {
            string result = "";
            if (Wallets == null)
                result = "У вас нет адресов!";
            else
            {
                result = "Ваши адреса:";
                foreach (var wallet in Wallets)
                {
                    result += $"\n{wallet.Name} - {wallet.Addr}\n";
                }
            }

            return result;
        }
    }
    class Wallet
    {
        public string Addr { get; set; }
        public string Name { get; set; }
        public Wallet() { }
        public Wallet(string addr, string name)
        {
            this.Addr = addr;
            this.Name = name;
        }
    }
}
