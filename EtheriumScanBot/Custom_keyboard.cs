﻿using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace EtheriumScanBot
{
    class Custom_keyboard
    {
        public static async void Add(string button1, string button2, string button3, string button4, string button5, ChatId id, string textmessage = "")
        {
            var keyboard = new ReplyKeyboardMarkup(new[]
            {
                new []
                {
                    new KeyboardButton(button1),
                    new KeyboardButton(button2),
                },
                new []
                {
                    new KeyboardButton(button3),
                    new KeyboardButton(button4),
                },
                new[]
                {
                    new KeyboardButton(button5),
                }
            }, resizeKeyboard: true);
            if (textmessage == "") ;
            else
                await Program.Bot.SendTextMessageAsync(id, textmessage, ParseMode.Markdown, replyMarkup: keyboard);
        }
        public static async void Add(string button1, string button2, string button3, string button4, ChatId id, string textmessage = "")
        {
            var keyboard = new ReplyKeyboardMarkup(new[]
            {
                new []
                {
                    new KeyboardButton(button1),
                    new KeyboardButton(button2),
                },
                new []
                {
                    new KeyboardButton(button3),
                    new KeyboardButton(button4),
                }

            }, resizeKeyboard: true);
            if (textmessage == "") ;
            else
                await Program.Bot.SendTextMessageAsync(id, textmessage, ParseMode.Markdown, replyMarkup: keyboard);
        }
        public static async void Add(string button1, string button2, string button3, ChatId id, string textmessage = "")
        {
            var keyboard = new ReplyKeyboardMarkup(new[]
                {
                new []
                {
                    new KeyboardButton(button1),
                    new KeyboardButton(button2),
                },
                new []
                {
                    new KeyboardButton(button3),
                }

            }, resizeKeyboard: true);
            if (textmessage == "") ;
            else
                await Program.Bot.SendTextMessageAsync(id, textmessage, ParseMode.Markdown, replyMarkup: keyboard);

        }
        public static async void Add(string button1, string button2, ChatId id, string textmessage = "")
        {
            var keyboard = new ReplyKeyboardMarkup(new[]
            {
                new []
                {
                    new KeyboardButton(button1),
                    new KeyboardButton(button2),
                },
            }, resizeKeyboard: true);

            if (textmessage == "") ;
            else
                await Program.Bot.SendTextMessageAsync(id, textmessage, replyMarkup: keyboard);
        }
        public static async void Add(string button1, ChatId id, string textmessage = "")
        {
            var keyboard = new ReplyKeyboardMarkup(new[]
            {
                new []
                {
                    new KeyboardButton(button1),
                },
            }, resizeKeyboard: true);

            if (textmessage == "") ;
            else
                await Program.Bot.SendTextMessageAsync(id, textmessage, ParseMode.Markdown, replyMarkup: keyboard);
        }
    }
    class Keyboard
    {
        public InlineKeyboardMarkup key { get; set; }

        public void Add(string button1, string button2)
        {
            var keyboard = new InlineKeyboardMarkup(new[]
          {
                new []
                {
                    new InlineKeyboardButton(button1, "ETH"),
                    new InlineKeyboardButton(button2, "BTC"),
                },
            });
            key = keyboard;
        }
    }
}
